const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const app = express();
const port = 8000;
const pass = 'pass';

app.get('/:name', (req, res) => {
    res.send(req.params.name);
});

app.get('/encode/:name', (req, res) => {
    res.send(Vigenere.Cipher(pass).crypt(req.params.name));
});

app.get('/decode/:name', (req, res) => {
    res.send(Vigenere.Decipher(pass).crypt(req.params.name));
});

app.listen(port, () => {
    console.log('We are live on ' + port);
});